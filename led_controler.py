#!/usr/bin/env python
# -*- coding:utf-8 -*- 

##############################################################################
## license :
##============================================================================
##
## File :        led_controler.py
## 
## Project :     led_controler
##
## This file is part of Tango device class.
## 
## Tango is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Tango is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Tango.  If not, see <http://www.gnu.org/licenses/>.
## 
##
## $Author :      damien.jeangerard$
##
## $Revision :    1
##
## $Date :        2021-05-06
##
## $HeadUrl :     $
##============================================================================
##            This file is generated by POGO
##    (Program Obviously used to Generate tango Object)
##
##        (c) - Software Engineering Group - ESRF
##############################################################################

"""Gestion du beamstop en fonction de la position du detecteur"""
from __future__ import print_function

from builtins import str
__all__ = ["led_controler", "led_controlerClass", "main"]

__docformat__ = 'restructuredtext'

import PyTango
import sys
from socket import socket, AF_INET, SOCK_DGRAM
import time

#sys.path.insert(0, '/nfs/ruche-proxima2a/proxima2a-soleil/gitRepos')
##sys.path.insert(0, '/nfs/ruche/proxima2a-soleil/gitRepos/')
#from beam_center import beam_center
#bc = beam_center()

# Add additional import
#----- PROTECTED REGION ID(led_controler.additionnal_import) ENABLED START -----#

#----- PROTECTED REGION END -----#	//	led_controler.additionnal_import

## Device States Description
## STANDBY :
## ON :
## OFF :
## ALARM :

class led_controler (PyTango.Device_4Impl):

    #--------- Add you global variables here --------------------------
    #----- PROTECTED REGION ID(led_controler.global_variables) ENABLED START -----#

    #----- PROTECTED REGION END -----#	//	led_controler.global_variables

    def __init__(self,cl, name):
        PyTango.Device_4Impl.__init__(self,cl,name)
        self.debug_stream("In __init__()")

        #led_controler.init_device(self)
        self.init_device()

        #----- PROTECTED REGION ID(led_controler.__init__) ENABLED START -----#
        print ("in init")
        self.last_command = None
        #----- PROTECTED REGION END -----#	//	led_controler.__init__

    def delete_device(self):
        self.debug_stream("In delete_device()")
        print('delete') 
        #----- PROTECTED REGION ID(led_controler.delete_device) ENABLED START -----#

        #----- PROTECTED REGION END -----#	//	led_controler.delete_device

    def init_device(self):
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())

        #----- PROTECTED REGION ID(led_controler.init_device) ENABLED START -----#

        self.attr_brightness_read = 10
        self.attr_sector_read = '0'
        self.attr_color_read = 'w'
        self.led_power = 'ON'

        # Parametre envoi UDP
        self.sock_send  = socket(AF_INET, SOCK_DGRAM)
        self.adr_send   = (self.address_ip_controler, int(self.port_controler))

        # Parametre reception du retour de la requete UDP
        adr_back   = ('',int(self.port_device_tango))
        self.sock_rec   = socket(AF_INET, SOCK_DGRAM)
        self.sock_rec.bind(adr_back)
        self.sock_rec.settimeout(1)

        print('adr_send    :',self.adr_send)
        print('adr_back    :', adr_back)
        print('nbr_de_leds :', self.nbr_de_leds)

        self.send_command_udp()
        print ('ok')

        #----- PROTECTED REGION END -----#	//	led_controler.init_device

#------------------------------------------------------------------
#       State command:
#       Description: met a jour le State et Status
#------------------------------------------------------------------

    def stateDev(self,real):
        devValues = {'0': 'ON',
        '1': 'OFF',
        '2': 'CLOSE',
        '3': 'OPEN',
        '4': 'INSERT',
        '5': 'EXTRACT',
        '6': 'MOVING',
        '7': 'STANDBY',
        '8': 'FAULT',
        '9': 'INIT',
        '10': 'RUNNING',
        '11': 'ALARM',
        '12': 'DISABLE',
        '13': 'UNKNOWN'}

        return devValues[str(real)]

    #deffined and apply the devcie State
    def defined_State(self,state):

        if state == 'ON':
            self.set_state(PyTango.DevState.ON)
        elif state == 'OFF':
            self.set_state(PyTango.DevState.OFF)
        elif state == 'ALARM':
          self.set_state(PyTango.DevState.ALARM)
        elif state == 'INIT':
          self.set_state(PyTango.DevState.INIT)
        else:
            self.set_state(PyTango.DevState.FAULT)

    def maj_state(self):
        self.set_status('') #initialise le status

        if self.last_command == None:
            self.set_status('pas de comande lancee')
            state = 'INIT'
        elif  self.led_power == 'OFF':
            state = 'OFF'
        elif self.last_command == True:
            self.set_status('derniere commande Ok')
            state = 'ON'
        elif self.last_command == False:
            self.set_status('derniere commande Not Ok')
            state = 'ALARM'

        self.defined_State(state)

#------------------------------------------------------------------
#       State command:
#       Description: This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
#       argout: DevState        State Code
#------------------------------------------------------------------
    def dev_state(self):
        self.maj_state()
        argout = self.get_state()
        #self.set_state(argout)
        return argout

#------------------------------------------------------------------
#       Status command:
#       Description: This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
#       argout: ConstDevString  Status description
#------------------------------------------------------------------
    def dev_status(self):
        self.maj_state()
        self.the_status = self.get_status()
        #self.set_status(self.the_status)
        return self.the_status

    def always_executed_hook(self):
        self.debug_stream("In always_excuted_hook()")
        #----- PROTECTED REGION ID(led_controler.always_executed_hook) ENABLED START -----#

        #----- PROTECTED REGION END -----#	//	led_controler.always_executed_hook

    #-----------------------------------------------------------------------------
    #    led_controler read/write attribute methods
    #-----------------------------------------------------------------------------

    def read_brightness(self, attr):
        self.debug_stream("In read_brightness")
        #----- PROTECTED REGION ID(led_controler.brightness_read) ENABLED START -----#
        attr.set_value(self.attr_brightness_read)
        #----- PROTECTED REGION END -----#  //  led_controler.brightness_read 
    def write_brightness(self, attr):
        self.debug_stream("In write_brightness")
        #----- PROTECTED REGION ID(led_controler.brightness_write) ENABLED START -----#
        brightness = attr.get_write_value()
        if  brightness >= 0 and brightness <= 100:
            self.attr_brightness_read = brightness
            argout = self.send_command_udp()
        #----- PROTECTED REGION END -----#  //  led_controler.brightness_write     

    def read_sector(self, attr):
        self.debug_stream("In read_sector")
        #----- PROTECTED REGION ID(led_controler.sector_read) ENABLED START -----#
        attr.set_value(self.attr_sector_read)
        #----- PROTECTED REGION END -----#  //  led_controler.sector_read 
    def write_sector(self, attr):
        self.debug_stream("In write_sector")
        #----- PROTECTED REGION ID(led_controler.sector_write) ENABLED START -----#
        if attr.get_write_value() in ('0', '1','2', '3', '4'):
            self.attr_sector_read = attr.get_write_value()
            argout = self.send_command_udp()
        #----- PROTECTED REGION END -----#  //  led_controler.sector_write            

    def read_color(self, attr):
        self.debug_stream("In read_color")
        #----- PROTECTED REGION ID(led_controler.color_read) ENABLED START -----#
        attr.set_value(self.attr_color_read)
        #----- PROTECTED REGION END -----#  //  led_controler.color_read 
    def write_color(self, attr):
        self.debug_stream("In write_color")
        #----- PROTECTED REGION ID(led_controler.color_write) ENABLED START -----#
        if attr.get_write_value() in ('w', 'r','g', 'b'):
            self.attr_color_read = attr.get_write_value()
            argout = self.send_command_udp()
        #----- PROTECTED REGION END -----#  //  led_controler.color_write        

        #----- PROTECTED REGION ID(led_controler.initialize_dynamic_attributes) ENABLED START -----#

        #----- PROTECTED REGION END -----#	//	led_controler.initialize_dynamic_attributes

    def read_attr_hardware(self, data):
        self.debug_stream("In read_attr_hardware()")
        #----- PROTECTED REGION ID(led_controler.read_attr_hardware) ENABLED START -----#

        #----- PROTECTED REGION END -----#	//	led_controler.read_attr_hardware


    #-----------------------------------------------------------------------------
    #    led_controler command methods
    #-----------------------------------------------------------------------------

    def INIT(self):
        self.debug_stream("INIT()")
        #----- PROTECTED REGION ID(led_controler.INIT) ENABLED START -----#

        #----- PROTECTED REGION END -----#      //      led_controler.INIT

        self.init_device() # ???
        argout = 'ttttt'

        return argout

    def is_INIT_allowed(self):
        return True

    def OFF(self):
        self.debug_stream("OFF()")
        #----- PROTECTED REGION ID(led_controler.OFF) ENABLED START -----#

        self.led_power = 'OFF'
        argout = self.send_command_udp(c='0')

        #----- PROTECTED REGION END -----#      //      led_controler.OFF
        return argout
    def is_OFF_allowed(self):
        return True

    def ON(self):
        self.debug_stream("ON()")
        #----- PROTECTED REGION ID(led_controler.ON) ENABLED START -----#
        
        self.led_power = 'ON'
        argout = self.send_command_udp()

        #----- PROTECTED REGION END -----#      //      led_controler.ON
        return argout
    def is_ON_allowed(self):
        return True

    #-----------------------------------------------------------------------------
    #    led_controler fonction Perso #########################################
    #-----------------------------------------------------------------------------

    def generate_message(self, a=None, b=None, c=None, d=None):
        # a = nbr de leds
        # b = coleur
        # c = brightness
        # d = secteur
        if a == None:
            a = str(int(self.nbr_de_leds))
        a1 = ['0','0']
        for i in enumerate(a[::-1]):
            a1[i[0]] = i[1]
        a = str.join('',a1)[::-1]

        if b == None:
            b = self.attr_color_read
        if b == 'w':
            b = '1'
        elif b == 'r':
            b = '2'
        elif b == 'g':
            b = '3'
        elif b == 'b':
            b = '4'

        if self.led_power == 'OFF':
            c = '0'
        print (c)
        if c == None :
            c = str(int(self.attr_brightness_read*2.55))

        c1 = ['0','0','0']
        for i in enumerate(c[::-1]):
            c1[i[0]] = i[1]
        c = str.join('',c1)[::-1]

        if d == None:
            d = self.attr_sector_read

        message  = a+b+c+d
        return message

    def send_command_udp(self, a=None, b=None, c=None, d=None):
        message = self.generate_message(a,b,c,d)

        self.sock_send.sendto( bytes(message,"utf-8"), self.adr_send ) # envoi du message
        time.sleep(0.5)
        print ('\nMessage sendto : %s' % message)

        try:                                            # attente retour du controleur
            print ('Ready for message')
            msg, addr = self.sock_rec.recvfrom(4096)
            print((msg,addr))
            argout = msg.decode('utf-8')
            #if msg.decode('utf-8') == message:
            if argout == message:
                self.last_command = True
            else:
                self.last_command = False
        except:                                     # si pas de retour dans un delay deffini par settimeout(x)
            #print('Not message on PORT %s %s' % adr_back)
            argout = 'except'
            self.last_command = False
        
        return argout
    
    #def posiBST3(self,outarg):
        #self.dev_BST3Tx.position, self.dev_BST3Tz.position  = outarg[0],outarg[1] #positionner le BST3 sur le faisceau axe Tx et Tz

class led_controlerClass(PyTango.DeviceClass):
    #--------- Add you global class variables here --------------------------
    #----- PROTECTED REGION ID(led_controler.global_class_variables) ENABLED START -----#

    #----- PROTECTED REGION END -----#	//	led_controler.global_class_variables

    def dyn_attr(self, dev_list):
        """Invoked to create dynamic attributes for the given devices.
        Default implementation calls
        :meth:`led_controler.initialize_dynamic_attributes` for each device

        :param dev_list: list of devices
        :type dev_list: :class:`PyTango.DeviceImpl`"""

        for dev in dev_list:
            try:
                dev.initialize_dynamic_attributes()
            except:
                import traceback
                dev.warn_stream("Failed to initialize dynamic attributes")
                dev.debug_stream("Details: " + traceback.format_exc())
        #----- PROTECTED REGION ID(led_controler.dyn_attr) ENABLED START -----#

        #----- PROTECTED REGION END -----#	//	led_controler.dyn_attr

    #    Class Properties
    class_property_list = {
        }

    #    Device Properties
    device_property_list = {
        'nbr_de_leds':
            [PyTango.DevDouble,
            "nombre de led presentes",
            [] ],
        'address_ip_controler':
            [PyTango.DevString,
            "adresse IP du controleur",
            [] ],
        'port_controler':
            [PyTango.DevDouble,
            "port du controleur",
            [] ],
        'port_device_tango':
            [PyTango.DevDouble,
            "port du device led_controler",
            [] ],  
        }

    #    Command definitions
    cmd_list = {
        'INIT':
            [[PyTango.DevVoid, "initialiseles device"],
            [PyTango.DevVoid, "none"]],
        'ON':
            [[PyTango.DevVoid, "allume l'anneau"],
            [PyTango.DevVoid, "none"]],
        'OFF':
            [[PyTango.DevVoid, "éteint l'anneau"],
            [PyTango.DevVoid, "none"]],
        }

    #    Attribute definitions
    attr_list = {
        'brightness'    : [[PyTango.DevDouble, PyTango.SCALAR, PyTango.READ_WRITE]],
        'sector'     : [[PyTango.DevString, PyTango.SCALAR, PyTango.READ_WRITE]],
        'color'    : [[PyTango.DevString, PyTango.SCALAR, PyTango.READ_WRITE]],
        }

def main():
    try:
        py = PyTango.Util(sys.argv)
        py.add_class(led_controlerClass,led_controler,'led_controler')

        U = PyTango.Util.instance()
        U.server_init()
        U.server_run()

    except PyTango.DevFailed as e:
        print('-------> Received a DevFailed exception:',e)
    except Exception as e:
        print('-------> An unforeseen exception occured....',e)

if __name__ == '__main__':
    main()
